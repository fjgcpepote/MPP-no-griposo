CC=gcc
CFLAGS=-Wall -Werror -O2 -Wno-maybe-uninitialized
LDFLAGS=-lm
SECUENCIAL=secuencial
OPENMP=openmp
MPI=mpi
MPIOPENMP=mpiopenmp
BASE=base
ODIR=obj
SRCDIR=src
BINDIR=bin
OBJETIVO=mpiopenmp

ifeq (${OBJETIVO}, ${SECUENCIAL})
MACRO= -D SECUENCIAL
else ifeq (${OBJETIVO}, ${OPENMP})
MACRO= -D OPENMP
CFLAGS:= $(CFLAGS) -fopenmp
LDFLAGS:= $(LDFLAGS) -fopenmp
else ifeq (${OBJETIVO}, ${MPI})
MACRO= -D MPI
CC= mpicc
else ifeq (${OBJETIVO}, ${MPIOPENMP})
MACRO= -D MPIOPENMP
CC= mpicc
CFLAGS:= $(CFLAGS) -fopenmp
LDFLAGS:= $(LDFLAGS) -fopenmp
endif

all: prerrequisitos $(BINDIR)/$(OBJETIVO)

$(BINDIR)/$(OBJETIVO): $(ODIR)/$(BASE).o $(ODIR)/$(OBJETIVO).o
	$(CC) $(MACRO) $(SRCDIR)/$(OBJETIVO).c $(SRCDIR)/$(BASE).c $(LDFLAGS) -o $@

$(ODIR)/$(OBJETIVO).o: $(SRCDIR)/$(OBJETIVO).c
	$(CC) $(CFLAGS) -c $(SRCDIR)/$(OBJETIVO).c -o $@

$(ODIR)/$(BASE).o: $(SRCDIR)/$(BASE).c
	$(CC) $(CFLAGS) $(MACRO) -c $(SRCDIR)/$(BASE).c -o $@

prerrequisitos:
	@mkdir -p $(ODIR)
	@mkdir -p $(BINDIR)

.DEFAULT:
	@echo "$< no se reconoce como orden"
	@echo "Uso \"make\" o \"make all\" para compilar el objetivo actual del Makefile ($(OBJETIVO))"
	@echo "Posibles opciones para objetivo: \"$(SECUENCIAL)\", \"$(OPENMP)\", \"$(MPI)\" y \"$(MPIOPENMP)\""
	@echo "\"make clean\" para eliminar datos residuales"

.PHONY: clean
clean:
	rm -rf *.gch *~ $(ODIR) core $(BINDIR)
