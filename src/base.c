#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h> //gettimeofday

#if defined MPI || defined MPIOPENMP
	#include <mpi.h>
#endif

#define _XOPEN_SOURCE

#define MAX_ITERACIONES 300
#define NUM_BUSQUEDAS 1000
#define A_SIZE 18
#define Y_SIZE 600
#define NUM_COLUMNAS 3
#define TIEMPO 2
#define NUM_HILOS 4

#ifdef SECUENCIAL
	extern void secuencial(double *Y, int tiempo, double * A, double min1, double max1, double min2, double max2, int aSize, double minError, int numBusquedas, unsigned int seed);
#elif OPENMP
	extern void openmp(double *Y, int tiempo, double * A, double min1, double max1, double min2, double max2, int aSize, double minError, int numBusquedas, unsigned int seed, int nt);
#elif MPI
	extern void mpi(double *Y, int tiempo, double * A, double min1, double max1, double min2, double max2, int aSize, int ySize, double minError, int numBusquedas, unsigned int seed, int nodo, int np);
#elif MPIOPENMP
	extern void mpiopenmp(double *Y, int tiempo, double * A, double min1, double max1, double min2, double max2, int aSize, int ySize, double minError, int numBusquedas, unsigned int seed, int nodo, int np, int nt);
#endif

//Lee un double imprimiendo el mensaje pasado como parámetro previamente
void leeValor(double* valor, const char* mensaje){

	int leidoCorrectamente=0;
	while(!leidoCorrectamente){
		printf("%s",mensaje);
		fflush(stdout);
		if(scanf("%lf", valor)==1 && getchar() == '\n')
			leidoCorrectamente++;
		else {
			fprintf(stderr, "ERROR leyendo de la entrada estándar, se esperaba un número decimal\n");
			fflush(stderr);
			while(getchar() != '\n');
		}
	}
}

//Funcion encargada de obtener los datos de entrada
void leeEntrada(double* min1,double* max1,double* min2,double* max2,double* v1,double* v2){

	int primeraVez=0,i;
	do{
		if(!primeraVez) primeraVez++;
		else {
			fprintf(stderr,"El límite superior (%lf) ha de ser mayor que el límite inferior (%lf)\n",*max1,*min1);
			fflush(stderr);
		}

		leeValor(min1,"Establezca el límite inferior para la primera parte de A: ");
		leeValor(max1,"Establezca el límite superior para la primera parte de A: ");
	}
	while(*max1 < *min1);

	primeraVez--;
	do{
		if(!primeraVez) primeraVez++;
		else {
			fprintf(stderr,"El límite superior (%lf) ha de ser mayor que el límite inferior (%lf)\n",*max2,*min2);
			fflush(stderr);
		}

		leeValor(min2,"Establezca el límite inferior para la primera parte de A: ");
		leeValor(max2,"Establezca el límite superior para la primera parte de A: ");
	}
	while(*max2 < *min2);

	for(i=0; i<NUM_COLUMNAS; i++){
		char tmp[64];
		sprintf(tmp, "Introduzca el valor %d-ésimo del primer vector: ", i);
		leeValor(&v1[i],tmp);
	}

	for(i=0; i<NUM_COLUMNAS; i++){
		char tmp[64];
		sprintf(tmp, "Introduzca el valor %d-ésimo del segundo vector: ", i);
		leeValor(&v2[i],tmp);
	}
}

//Funcion que imprime la matriz A y el mensaje recibido como parametro
void imprimirA(double* A, const char* mensaje){

	int i,j;
	printf("%s",mensaje);
	for(i=0; i<A_SIZE; i+=NUM_COLUMNAS){
		for(j=0; j<NUM_COLUMNAS; j++)
			printf("%lf ",A[i+j]);
		printf("\n");
	}
	fflush(stdout);
}

//Funcion encargada de generar A a partir de los intervalos y de completar Y
void generar(double* A, double* Y, double min1, double max1, double min2, double max2, double* v1, double* v2, unsigned int seed){

	int i,j;
	//Generamos un valor de cada submatriz de A por iteracion
	//siguiendo la formula minimo + [0,1]*(maximo-minimo)
	for(i=0;i<A_SIZE/2;i++){

		A[i] = min1 + (double)rand_r(&seed) / (double)RAND_MAX * (max1 - min1);
		seed+=A[i];
		A[A_SIZE/2+i] = min2 + (double)rand_r(&seed) / (double)RAND_MAX * (max2 - min2);
		seed+=A[A_SIZE/2+i];
	}

	//Copiamos las filas proporcionadas
	memcpy(Y, v1, NUM_COLUMNAS*sizeof(double));
	memcpy(Y+NUM_COLUMNAS, v2, NUM_COLUMNAS*sizeof(double));

	//Empezamos en la segunda fila
	for(i=TIEMPO*NUM_COLUMNAS;i<Y_SIZE;i+=NUM_COLUMNAS)

		for(j=0;j<NUM_COLUMNAS;j++){

			double Y1=Y[i+j-2*NUM_COLUMNAS];
			double Y2=Y[i+j-NUM_COLUMNAS];
			//Calculamos el valor
			Y[i+j] = Y1*A[j] + Y1*A[j+NUM_COLUMNAS] + Y1*A[j+2*NUM_COLUMNAS] + Y2*A[j+3*NUM_COLUMNAS] + Y2*A[j+4*NUM_COLUMNAS] + Y2*A[j+5*NUM_COLUMNAS];
			//Perturbamos el valor sumando al resultado (-0.01+[0,1]*0.02)*resultado
			//asi obtenemos una variacion del 0 al 1 por ciento positiva o negativa
			Y[i+j] += (-0.01 + (double)rand_r(&seed) / (double)RAND_MAX * (0.02) )* Y[i+j];
			seed+=Y[i+j];
		}
}

//Funcion encargada de crear un individuo
void inicializar(double* A, double min1, double max1, double min2, double max2, int nodo, unsigned int seed, int numBusqueda){

	int i;
	//Para cada submatriz obtenemos el valor de la decima parte
	//del intervalo en el que pueden estar los valores
	double A1intervalo = (max1-min1)/10;
	double A2intervalo = (max2-min2)/10;

	//Por cada iteracion calculamos un valor de cada submatriz
	//siguiendo la formula minimo + [0,10]*intervalo
	//pudiendo oscilar entre el maximo y el minimo permitidos
	for(i=0;i<A_SIZE/2;i++){

		A[i] = min1+rand_r(&seed)%10*A1intervalo;
		seed+=nodo+A[i]*numBusqueda;
		A[A_SIZE/2+i] = min2+rand_r(&seed)%10*A2intervalo;
		seed+=nodo+A[A_SIZE/2+i]*numBusqueda;
	}
}

//Funcion que compara la norma entre la matriz Y original y la matriz X
//obtenida a partir de la matriz A calculcada
double compararserie(double* Y, int tiempoSecuencial, double* A, int tiempoComparar){

	int i,j;
	double error=0.0;

	double* X=(double*) malloc(Y_SIZE*sizeof(double));

	//Copiamos las primeras tiempoSecuencial filas
	memcpy(X, Y, sizeof(double)*tiempoSecuencial*NUM_COLUMNAS);

	//Calculamos X desde la fila tiempoSecuencial hasta la fila tiempoComparar
	//(mismo procedimiento que en funcion generar, pero sin perturbar)
	for(i=tiempoSecuencial*NUM_COLUMNAS; i<tiempoComparar; i+=NUM_COLUMNAS)

		for(j=0;j<NUM_COLUMNAS;j++){

			double X1=X[i+j-2*NUM_COLUMNAS];
			double X2=X[i+j-NUM_COLUMNAS];
			X[i+j] = X1*A[j] + X1*A[j+NUM_COLUMNAS] + X1*A[j+2*NUM_COLUMNAS] + X2*A[j+3*NUM_COLUMNAS] + X2*A[j+4*NUM_COLUMNAS] + X2*A[j+5*NUM_COLUMNAS];
		}

	//Calculamos la norma entre las posiciones generadas de X e Y
	for(i=tiempoSecuencial*NUM_COLUMNAS; i<tiempoComparar; i++) error += (Y[i] - X[i]) * (Y[i] - X[i]);
	error = sqrt(error);

	free(X);

	return error;
}

//Funcion que comprueba si el algoritmo voraz debe terminar, acepta dos condiciones:
//1.- El error conseguido es mejor que el minimo error permitido
//2.- La iteracion actual es superior o igual que el limite de iteraciones
int esSolucion(int numIteracion, int tiempo, double* Y, double* A, double* error, double minError){

	return ((*error=compararserie(Y, tiempo, A, Y_SIZE)) < minError || numIteracion >= MAX_ITERACIONES);
}

//Funcion que mejora un individuo del algoritmo
void mejorar(double error, double errorAnt, double* A, double* Afluctua, int numIteracion, double min1, double max1, double min2, double max2){

	//Alteramos la posicion en funcion de la iteracion actual
	int posicion=numIteracion%A_SIZE;

	//Si el cambio anterior ha empeorado la configuracion, lo cancelamos e invertimos el signo.
	//Si el signo ya estaba invertido, reducimos el valor del cambio.
	if(errorAnt < error && errorAnt!=-1.0){
		int posicionAnterior = posicion-1;
		if(posicionAnterior<0) posicionAnterior+=A_SIZE;

		A[posicionAnterior]-=Afluctua[posicionAnterior];
		if(Afluctua[posicionAnterior]>0) Afluctua[posicionAnterior]*=-1.0;
		else Afluctua[posicionAnterior]*=-0.1;
	}

	//Si el valor actual esta fuera del intervalo y tiende a alejarse, invertimos el signo del cambio.
	//Asi limitamos errores
	if(posicion<A_SIZE/2){
		if(A[posicion]<min1 && Afluctua[posicion]<0) Afluctua[posicion]*=-1.0;
		if(A[posicion]>max1 && Afluctua[posicion]>0) Afluctua[posicion]*=-1.0;
	}
	else{
		if(A[posicion]<min2 && Afluctua[posicion]<0) Afluctua[posicion]*=-1.0;
		if(A[posicion]>max2 && Afluctua[posicion]>0) Afluctua[posicion]*=-1.0;
	}
	//Aplicamos el cambio a la posicion pertinente, el valor sumado ira cambiando con las iteraciones
	A[posicion]+=Afluctua[posicion];
}

//Funcion que calcula la norma entre la matriz A generada y la calculada
double compararmatriz(double* AGenerada,double* ACalculada){

	int i;
	double norma=0.0;

	for(i=0; i<A_SIZE; i++) norma += (AGenerada[i] - ACalculada[i]) * (AGenerada[i] - ACalculada[i]);
	norma = sqrt(norma);

	return norma;
}

int main(int argc, char* argv[]){

	double min1, max1, min2, max2, minError;
	double* Y;
	double* ACalculada;
	double* AGenerada;
	struct timeval inicioGTOD, finGTOD;

	//Inicializamos la semilla
	unsigned int seed=time(NULL);

	#if defined MPI || defined MPIOPENMP
		int nodo, np;
		//Inicializamos MPI
		MPI_Init(&argc,&argv);
		MPI_Comm_size(MPI_COMM_WORLD,&np);
		MPI_Comm_rank(MPI_COMM_WORLD,&nodo);
		//Alteramos la semilla para conseguir mas variedad
		seed+=nodo*np;
		if(!nodo){
	#endif

	//Procesamos la entrada y generamos las variables
	Y = (double*) malloc(Y_SIZE*sizeof(double));
	ACalculada = (double*) malloc(A_SIZE*sizeof(double));
	AGenerada = (double*) malloc(A_SIZE*sizeof(double));

	double* v1 = (double*) malloc(NUM_COLUMNAS*sizeof(double));
	double* v2 = (double*) malloc(NUM_COLUMNAS*sizeof(double));

	leeEntrada(&min1,&max1,&min2,&max2,v1,v2);
	minError=((max1-min1)+(max2-min2))*1000/MAX_ITERACIONES/NUM_BUSQUEDAS;

	generar(AGenerada,Y,min1,max1,min2,max2,v1,v2,seed);

	free(v1);
	free(v2);

	//Mostramos la matriz A original
	imprimirA(AGenerada,"\nA generada\n");

	//Toma de tiempo inicial
  gettimeofday(&inicioGTOD, NULL);

	#if defined MPI || defined MPIOPENMP
		}
	#endif

	//Ejecutamos la heuristica
	#ifdef SECUENCIAL
		secuencial(Y,TIEMPO,ACalculada,min1,max1,min2,max2,A_SIZE,minError,NUM_BUSQUEDAS,seed);
	#elif OPENMP
		openmp(Y,TIEMPO,ACalculada,min1,max1,min2,max2,A_SIZE,minError,NUM_BUSQUEDAS,seed,NUM_HILOS);
	#elif MPI
		mpi(Y,TIEMPO,ACalculada,min1,max1,min2,max2,A_SIZE,Y_SIZE,minError,NUM_BUSQUEDAS,seed,nodo,np);
	#elif MPIOPENMP
		mpiopenmp(Y,TIEMPO,ACalculada,min1,max1,min2,max2,A_SIZE,Y_SIZE,minError,NUM_BUSQUEDAS,seed,nodo,np,NUM_HILOS);
	#endif

	#if defined MPI || defined MPIOPENMP
		if(!nodo){
	#endif

	//Toma de tiempo final
  gettimeofday(&finGTOD, NULL);

	free(Y);

	//Mostramos la matriz A calculada
	imprimirA(ACalculada,"A calculada\n");

	double norma= compararmatriz(AGenerada,ACalculada);

	free(AGenerada);
	free(ACalculada);

	//Mostramos la norma entre las matrices A y el tiempo transcurrido
	printf("Norma: %lf\n",norma);

	double tiempoGTOD = ((double)finGTOD.tv_sec + (double)finGTOD.tv_usec/1000000) - ((double)inicioGTOD.tv_sec + (double)inicioGTOD.tv_usec/1000000);

  printf("Tiempo (gettimeofday) = %.6f''\n",tiempoGTOD);
	fflush(stdout);

	#if defined MPI || defined MPIOPENMP
		}
		//Finalizamos MPI
		MPI_Finalize();
	#endif

	return 0;
}
