#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

extern void inicializar(double* A, double min1, double max1, double min2, double max2, int nodo, unsigned int seed, int numBusqueda);
extern int esSolucion(int numIteracion, int tiempo, double* Y, double* A, double* error, double minError);
extern void mejorar(double error, double errorAnt, double* A, double* Afluctua, int numIteracion, double min1, double max1, double min2, double max2);

void mpi(double *Y, int tiempo, double * A, double min1, double max1, double min2, double max2, int aSize, int ySize, double minError, int numBusquedas, unsigned int seed, int nodo, int np){

	double* mejoresErroresLocales;
	if(!nodo){
		double aux[5]={ min1, max1, min2, max2, minError};
		MPI_Bcast(aux,5,MPI_DOUBLE,0,MPI_COMM_WORLD);
		mejoresErroresLocales = (double*) malloc(np*sizeof(double));
	}
	else {
		double aux[5];
		MPI_Bcast(aux,5,MPI_DOUBLE,0,MPI_COMM_WORLD);
		min1=aux[0];
		max1=aux[1];
		min2=aux[2];
		max2=aux[3];
		minError=aux[4];
		Y = (double*) malloc(ySize*sizeof(double));
	}

	MPI_Bcast(Y,ySize,MPI_DOUBLE,0,MPI_COMM_WORLD);

	double mejorErrorLocal=-1.0, mejorErrorGlobal=-1.0;
	double* AA = (double*) malloc(aSize*sizeof(double));
	double* AAA = (double*) malloc(aSize*sizeof(double));

	double* Afluctua=(double*)malloc(aSize*sizeof(double));
	double A1fluctua = (max1-min1)/10;
	double A2fluctua = (max2-min2)/10;

	int i,j,k;
	int mejorNodo=0;

	for(i=0; i<numBusquedas; i++){

		for(j=0;j<aSize/2;j++){

			Afluctua[j] = A1fluctua;
			Afluctua[aSize/2+j] = A2fluctua;
		}

		double errorActual, errorAnterior=-1.0;
		int numIteracion=0;

		inicializar(AAA,min1,max1,min2,max2,0,seed,i);

		while(!esSolucion(numIteracion, tiempo, Y, AAA, &errorActual, minError)){
			//No seleccionamos, ni combinamos, ni incluimos porque solo hay un elemento

			if(mejorErrorLocal == -1.0 || errorActual < mejorErrorLocal){
				mejorErrorLocal = errorActual;
				memcpy(AA, AAA, aSize*sizeof(double));
			}
			mejorar(errorActual, errorAnterior, AAA, Afluctua, numIteracion, min1, max1, min2, max2);
			errorAnterior=errorActual;
			numIteracion++;
		}

		if(errorActual < minError){
			mejorErrorLocal = errorActual;
			memcpy(AA, AAA, aSize*sizeof(double));
		}

		MPI_Gather(&mejorErrorLocal, 1, MPI_DOUBLE, mejoresErroresLocales, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

		if(!nodo){
			if(mejorErrorLocal<mejoresErroresLocales[mejorNodo])
				mejorNodo=0;
			for(k=1; k<np; k++)
				if(mejoresErroresLocales[k]<mejoresErroresLocales[mejorNodo])
					mejorNodo=k;
		}

		MPI_Bcast(&mejorNodo, 1, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(&mejorErrorLocal, 1, MPI_DOUBLE, mejorNodo, MPI_COMM_WORLD);

		if(mejorNodo==nodo && nodo)
			MPI_Send(AA, aSize, MPI_DOUBLE, 0, i, MPI_COMM_WORLD);

		if(!nodo && mejorNodo!=0)
			MPI_Recv(AA, aSize, MPI_DOUBLE, mejorNodo, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	free(Afluctua);
	free(AAA);

	if(!nodo){

		free(mejoresErroresLocales);

		if(mejorErrorGlobal == -1.0 || mejorErrorLocal < mejorErrorGlobal){
			mejorErrorGlobal= mejorErrorLocal;
			memcpy(A, AA, aSize*sizeof(double));
		}

		if (mejorErrorGlobal < minError) printf("\nSí ");
		else printf("\nNo ");

		printf("se cumplió el objetivo de minimizar el error: objetivo %lf, conseguido %lf\n",minError,mejorErrorGlobal);
		printf("La configuración pertenece al nodo %d\n\n", mejorNodo+1);
	}
	else free(Y);

	free(AA);
}
