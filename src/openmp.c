#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

extern void inicializar(double* A, double min1, double max1, double min2, double max2, int nodo, unsigned int seed, int numBusqueda);
extern int esSolucion(int numIteracion, int tiempo, double* Y, double* A, double* error, double minError);
extern void mejorar(double error, double errorAnt, double* A, double* Afluctua, int numIteracion, double min1, double max1, double min2, double max2);

void openmp(double *Y, int tiempo, double * A, double min1, double max1, double min2, double max2, int aSize, double minError, int numBusquedas, unsigned int seed, int nt){

	double mejorErrorLocal=-1.0, mejorErrorGlobal=-1.0;
	double* AA = (double*) malloc(aSize*sizeof(double));
	double* AAA = (double*) malloc(aSize*sizeof(double));

	double * Afluctua=(double*)malloc(aSize*sizeof(double));
	double A1fluctua = (max1-min1)/10;
	double A2fluctua = (max2-min2)/10;

	int mejorHilo;

  #pragma omp parallel num_threads(nt) firstprivate(Afluctua,AA,AAA,mejorErrorLocal)
  {
    int i,j;
    #pragma omp for
  	for(i=0; i<numBusquedas; i++){

  		for(j=0;j<aSize/2;j++){

  			Afluctua[j] = A1fluctua;
  			Afluctua[aSize/2+j] = A2fluctua;
  		}

  		double errorActual, errorAnterior=-1.0;
  		int numIteracion=0;

  		inicializar(AAA,min1,max1,min2,max2,0,seed,i);

  		while(!esSolucion(numIteracion, tiempo, Y, AAA, &errorActual, minError)){
  			//No seleccionamos, ni combinamos, ni incluimos porque solo hay un elemento

  			if(mejorErrorLocal == -1.0 || errorActual < mejorErrorLocal){
  				mejorErrorLocal = errorActual;
  				memcpy(AA, AAA, aSize*sizeof(double));
  			}
  			mejorar(errorActual, errorAnterior, AAA, Afluctua, numIteracion, min1, max1, min2, max2);
  			errorAnterior=errorActual;
  			numIteracion++;
  		}

  		if(errorActual < minError){
  			mejorErrorLocal = errorActual;
  			memcpy(AA, AAA, aSize*sizeof(double));
  		}
  	}

    #pragma omp critical
  	if(mejorErrorGlobal == -1.0 || mejorErrorLocal < mejorErrorGlobal){
  		mejorErrorGlobal= mejorErrorLocal;
  		memcpy(A, AA, aSize*sizeof(double));
			mejorHilo=omp_get_thread_num();
  	}
  }

	free(Afluctua);
	free(AA);
	free(AAA);

	if (mejorErrorGlobal < minError) printf("\nSí ");
	else printf("\nNo ");

	printf("se cumplió el objetivo de minimizar el error: objetivo %lf, conseguido %lf\n",minError,mejorErrorGlobal);
	printf("La configuración pertenece al hilo %d\n\n",mejorHilo+1);
}
