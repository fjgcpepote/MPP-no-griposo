#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <omp.h>

extern void inicializar(double* A, double min1, double max1, double min2, double max2, int nodo, unsigned int seed, int numBusqueda);
extern int esSolucion(int numIteracion, int tiempo, double* Y, double* A, double* error, double minError);
extern void mejorar(double error, double errorAnt, double* A, double* Afluctua, int numIteracion, double min1, double max1, double min2, double max2);

void mpiopenmp(double *Y, int tiempo, double * A, double min1, double max1, double min2, double max2, int aSize, int ySize, double minError, int numBusquedas, unsigned int seed, int nodo, int np, int nt){

	double* mejoresErroresGlobales;
	if(!nodo){
		double aux[5]={ min1, max1, min2, max2, minError};
		MPI_Bcast(aux,5,MPI_DOUBLE,0,MPI_COMM_WORLD);
		mejoresErroresGlobales = (double*) malloc(np*sizeof(double));
	}
	else {
		double aux[5];
		MPI_Bcast(aux,5,MPI_DOUBLE,0,MPI_COMM_WORLD);
		min1=aux[0];
		max1=aux[1];
		min2=aux[2];
		max2=aux[3];
		minError=aux[4];
		Y = (double*) malloc(ySize*sizeof(double));
		A = (double*) malloc(aSize*sizeof(double));
	}

	MPI_Bcast(Y,ySize,MPI_DOUBLE,0,MPI_COMM_WORLD);

	double mejorErrorLocal=-1.0, mejorErrorGlobal=-1.0;
	double* AA = (double*) malloc(aSize*sizeof(double));
	double* AAA = (double*) malloc(aSize*sizeof(double));

	double * Afluctua=(double*)malloc(aSize*sizeof(double));
	double A1fluctua = (max1-min1)/10;
	double A2fluctua = (max2-min2)/10;

	int mejorHilo;
	int mejorNodo=0;

  #pragma omp parallel num_threads(nt) firstprivate(Afluctua,AA,AAA,mejorErrorLocal)
  {
    int i,j;
    #pragma omp for
  	for(i=0; i<numBusquedas; i++){

  		for(j=0;j<aSize/2;j++){

  			Afluctua[j] = A1fluctua;
  			Afluctua[aSize/2+j] = A2fluctua;
  		}

  		double errorActual, errorAnterior=-1.0;
  		int numIteracion=0;

  		inicializar(AAA,min1,max1,min2,max2,0,seed,i);

  		while(!esSolucion(numIteracion, tiempo, Y, AAA, &errorActual, minError)){
  			//No seleccionamos, ni combinamos, ni incluimos porque solo hay un elemento

  			if(mejorErrorLocal == -1.0 || errorActual < mejorErrorLocal){
  				mejorErrorLocal = errorActual;
  				memcpy(AA, AAA, aSize*sizeof(double));
  			}
  			mejorar(errorActual, errorAnterior, AAA, Afluctua, numIteracion, min1, max1, min2, max2);
  			errorAnterior=errorActual;
  			numIteracion++;
  		}

  		if(errorActual < minError){
  			mejorErrorLocal = errorActual;
  			memcpy(AA, AAA, aSize*sizeof(double));
  		}
  	}

    #pragma omp critical
  	if(mejorErrorGlobal == -1.0 || mejorErrorLocal < mejorErrorGlobal){
  		mejorErrorGlobal= mejorErrorLocal;
  		memcpy(A, AA, aSize*sizeof(double));
		mejorHilo=omp_get_thread_num();
  	}
  }

	free(Afluctua);
	free(AA);
	free(AAA);

	MPI_Gather(&mejorErrorGlobal, 1, MPI_DOUBLE, mejoresErroresGlobales, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	if(!nodo){
		int k;
		for(k=1; k<np; k++)
			if(mejoresErroresGlobales[k]<mejoresErroresGlobales[mejorNodo])
				mejorNodo=k;
		mejorErrorGlobal=mejoresErroresGlobales[mejorNodo];
		free(mejoresErroresGlobales);
	}

	MPI_Bcast(&mejorNodo, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if(mejorNodo==nodo && nodo){
		MPI_Send(A, aSize, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
		MPI_Send(&mejorHilo, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);
	}

	if(!nodo && mejorNodo!=0){
		MPI_Recv(A, aSize, MPI_DOUBLE, mejorNodo, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&mejorHilo, 1, MPI_INT, mejorNodo, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	if(!nodo){

		if (mejorErrorGlobal < minError) printf("\nSí ");
		else printf("\nNo ");

		printf("se cumplió el objetivo de minimizar el error: objetivo %lf, conseguido %lf\n",minError,mejorErrorGlobal);
		printf("La configuración pertenece al hilo %d del nodo %d\n\n",mejorHilo+1,mejorNodo+1);
	}
	else{

		free(A);
		free(Y);
	}
}
